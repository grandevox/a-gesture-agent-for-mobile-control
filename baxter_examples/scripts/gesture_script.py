#!/usr/bin/env python

# RMIT UNIVERSITY, PROGRAMMING PROJECT-1, CAPSOTONE PROJECT
# PROJECT TITLE : A GESTURE AGENT FOR MOBILE ROBOT
# AUTHOR: ABHIJEET NAIR (S3400586)
# SUPERVISOR(S): DR. IAN PEAK (VX LAB, RMIT UNIVERSITY)
#               A/PROF. JAMES HARLAND (RMIT UNIVERSITY)
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted for educational use and study purposes only


import argparse
import math
import random
import time
import threading
from std_msgs.msg import String

import rospy

from std_msgs.msg import (
    UInt16,
)

import baxter_interface

from baxter_interface import CHECK_VERSION

# Gesture class contains all the gestures as functions
class Gesture(object):

    def __init__(self):
        self._pub_rate = rospy.Publisher('robot/joint_state_publish_rate',
                                         UInt16, queue_size=10) #Sets the rate at which commands gets published
        self._left_arm = baxter_interface.limb.Limb("left") #Getting control of the left arm
        self._right_arm = baxter_interface.limb.Limb("right") #Getting control of the right arm
        self._left_joint_names = self._left_arm.joint_names() #Getting the left joint names
        self._right_joint_names = self._right_arm.joint_names() #Getting the names of the right joint

        # control parameters
        self._rate = 500.0  # Hz
        self._left_arm.set_joint_position_speed(0.5) # Adjust the speed of the right arm
        self._right_arm.set_joint_position_speed(0.5) # Adjust the speed of the left arm

        print("Getting robot state... ")
        self._rs = baxter_interface.RobotEnable(CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()
       
    def _reset_control_modes(self):
        rate = rospy.Rate(self._rate)
        for _ in xrange(100):
            if rospy.is_shutdown():
                return False
            self._left_arm.exit_control_mode()
            self._right_arm.exit_control_mode()
            self._pub_rate.publish(100)  # 100Hz default joint state rate
            rate.sleep()
        return True

    def set_neutral(self):
        """
        Sets both arms and the head back into a neutral pose.
        """
        print("Moving to neutral pose...")
        self._left_arm.move_to_neutral()
        self._right_arm.move_to_neutral()
        baxter_interface.Head().set_pan(0.0, speed=0.5, timeout=10) # slow head movement


    def clean_shutdown(self):
        """
        Performs disabiling of motors and returining to normal position prior to shutdown
        """
        print("\nExiting example...")
        #return to normal
        self._reset_control_modes()
        self.set_neutral()
        if not self._init_state:
            print("Disabling robot...")
            self._rs.disable()
        return True

    def wave(self):
        #self.set_neutral()
        """
        Performs a royal wave
        """
        limb = baxter_interface.Limb('right')
        limb.set_joint_position_speed(0.6)
	wave_1 = {'right_s0': -0.459, 'right_s1': -0.202, 'right_e0': 1.807, 'right_e1': 1.714, 'right_w0': -0.906, 'right_w1': -1.545, 'right_w2': -0.276}
	wave_2 = {'right_s0': -0.395, 'right_s1': -0.202, 'right_e0': 1.831, 'right_e1': 1.981, 'right_w0': -1.979, 'right_w1': -1.100, 'right_w2': -0.448}
		
        for _move in range(3):
            limb.move_to_joint_positions(wave_1, timeout=3, threshold=0.1)
            limb.move_to_joint_positions(wave_2,timeout=2, threshold=0.1)

        self.set_neutral() # Set the hand back to neutral position 

    def bigwave(self): 
 
        """
        Performs an exagerrated wave using the left arm with the s0 joint
        """
        limb = baxter_interface.Limb('left')
        limb.set_joint_position_speed(0.5)
        wave_1 = {'left_s0': 0.0, 'left_s1': -1.3, 'left_e0': 0.0, 'left_e1': 0.0, 'left_w0': -1.8, 'left_w1': 1.2, 'left_w2': -1.8}
        wave_2 = {'left_s0': 0.0, 'left_s1': -1.3, 'left_e0': 0.0, 'left_e1': 0.0, 'left_w0': -1.8, 'left_w1':0, 'left_w2': -1.8}
		
        # Timeout - Shown in the command below allows us to move to the 
        # next position quickly without any delay (reduces the time spent on a pose)
        # Threshold - Determines the accuracy of the gesture (joint postions)
        for _move in range(3):
            limb.move_to_joint_positions(wave_1, timeout=1.5, threshold=0.008)
            limb.move_to_joint_positions(wave_2,timeout=1.5, threshold=0.008)
        
        self.set_neutral() # Set the hand back to neutral position 
    
    def nod(self):
        """
        Performs a nod where the head moves up and down two times in a row
        """
        for _move in range(2):
            baxter_interface.Head().command_nod()
        self.set_neutral()


    def sayno(self):
        """
        Shakes head to say no or refuse the user 
        """
        for _move in range(2):
            baxter_interface.Head().set_pan(-0.6, speed=1, timeout=10)
            baxter_interface.Head().set_pan(0.6, speed=1, timeout=10)
        self.set_neutral()

    def comehere(self):
        
        """
        Using its wrist pitch joint attempts to call the user towards the robot
        The wirst pitch joint moves back and forth 
        """
        limb = baxter_interface.Limb('left')
        limb.set_joint_position_speed(0.5)
        wave_1 = {'left_s0': -1, 'left_s1': 0.0, 'left_e0': -2.8, 'left_e1': 0.0, 'left_w0': 0.0, 'left_w1': 1.5, 'left_w2': 0.0}
        wave_2 = {'left_s0': -1, 'left_s1': 0.0, 'left_e0': -2.8, 'left_e1': 0.0, 'left_w0': 0.0, 'left_w1': 00, 'left_w2': 0.0}
		
        for _move in range(3):
            limb.move_to_joint_positions(wave_1,timeout=1.5, threshold=0.005)
            limb.move_to_joint_positions(wave_2,timeout=1.5, threshold=0.005)
        
        self.set_neutral()


    def firstbump(self):

        """
        Performs a fist bump using the right hand
        """
        # Get control of the right arm by accessing the Limb class
        # through the baxter interface
        limb = baxter_interface.Limb('right') 
        angles = limb.joint_angles()
        angles['right_s0']=1.0
        angles['right_s1']=-0.2
        angles['right_e0']=0.0
        angles['right_e1']=0.0
        angles['right_w0']=0.0
        angles['right_w1']=0.0
        angles['right_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles)

        angles = limb.joint_angles()
        angles['right_s0']=1.0
        angles['right_s1']=-0.2
        angles['right_e0']=0.0
        angles['right_e1']=0.0
        angles['right_w0']=0.0
        angles['right_w1']=0.0
        angles['right_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles)
        limb.move_to_neutral()


    def shakehands(self):

        """
        Performs a hand shake using the right arm
        """
        limb = baxter_interface.Limb('right')
        angles = limb.joint_angles()
        angles['right_s0']=1.0
        angles['right_s1']=0.0
        angles['right_e0']=1.6
        angles['right_e1']=0.0
        angles['right_w0']=0.0
        angles['right_w1']=0.0
        angles['right_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles) # moves the arm towards the user
        time.sleep(1)

        self.set_neutral()
    
    def performDAB(self):

        """
        Performs a DAB however in this gesture
        instructions are executed sequentially not concurrently 
        """
        limb = baxter_interface.Limb('left') # left arm movement
        angles = limb.joint_angles()
        angles['left_s0']=1.0
        angles['left_s1']=-0.4
        angles['left_e0']=-3.5
        angles['left_e1']=0.0
        angles['left_w0']=0.0
        angles['left_w1']=0.0
        angles['left_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles) # move to the defined position

        limb = baxter_interface.Limb('right') #right arm movement
        angles = limb.joint_angles()
        angles['right_s0']=1.0
        angles['right_s1']=-0.4
        angles['right_e0']=2.5
        angles['right_e1']=2.5
        angles['right_w0']=0.0
        angles['right_w1']=1.2
        angles['right_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles) # move to the defined position
        time.sleep(1)


    def rightarmwhy(self):

        """
        Performs why gesture for right arm
        """

        limb = baxter_interface.Limb('right')
        angles = limb.joint_angles()
        angles['right_s0']=-1.0
        angles['right_s1']=-0.4
        angles['right_e0']=3.5
        angles['right_e1']=0
        angles['right_w0']=0
        angles['right_w1']=0
        angles['right_w2']=0.0
        print(angles)
        #limb.move_to_joint_positions(angles,timeout=10, threshold=0.008)
        #time.sleep(1)  
        # The above delay can be uncommented for slower response time
        #angles = limb.joint_angles()
        angles['right_s0']=-1.0
        angles['right_s1']=1.0
        angles['right_e0']=3.5
        angles['right_e1']=2.0
        angles['right_w0']=0
        angles['right_w1']=-1
        angles['right_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles,timeout=10, threshold=0.008)
        limb.move_to_neutral()

    def leftarmwhy(self):
        self.set_neutral()
        """
        Sets the left arm in the correct position
        """
        limb = baxter_interface.Limb('left')
        limb.set_joint_position_speed(0.5)
        angles = limb.joint_angles()
        angles['left_s0']=1.0
        angles['left_s1']=-0.4
        angles['left_e0']=-3.5
        angles['left_e1']=0.0
        angles['left_w0']=0.0
        angles['left_w1']=0.0
        angles['left_w2']=0.0
        print(angles)
        angles = limb.joint_angles()
        angles['left_s0']=1.0
        angles['left_s1']=1.0
        angles['left_e0']=-3.5
        angles['left_e1']=2
        angles['left_w0']=0
        angles['left_w1']=-1
        angles['left_w2']=0
        print(angles)
        limb.move_to_joint_positions(angles,timeout=10, threshold=0.008)
        limb.move_to_neutral()
        

    def rightdab(self):
        #self.set_neutral()
        """
        Sets the right arm in the dab position
        """
        limb = baxter_interface.Limb('left')
        angles = limb.joint_angles()
        angles['left_s0']=1.0
        angles['left_s1']=-0.4
        angles['left_e0']=-3.5
        angles['left_e1']=0.0
        angles['left_w0']=0.0
        angles['left_w1']=0.0
        angles['left_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles)

    def leftdab(self):
        limb = baxter_interface.Limb('right')
        angles = limb.joint_angles()
        angles = limb.joint_angles()
        angles['right_s0']=0.0
        angles['right_s1']=0.0
        angles['right_e0']=2.5
        angles['right_e1']=2.5
        angles['right_w0']=0.0
        angles['right_w1']=0.0
        angles['right_w2']=0.0
        print(angles)
        limb.move_to_joint_positions(angles)


    def highfive(self):
        self.set_neutral()
        """
        Performs a high five
        """
        limb = baxter_interface.Limb('left')
        angles = limb.joint_angles()
        angles['left_s0']=-1
        angles['left_s1']=-0.9
        angles['left_e0']=0
        angles['left_e1']=1.0
        angles['left_w0']=0.0
        angles['left_w1']=-1.5
        angles['left_w2']=0.0
        print(angles)
        vel_0 = {'left_s0': 1, 'left_s1': 1, 'left_e0': 1, 'left_e1': 1, 'left_w0': 1, 'left_w1': 1, 'left_w2': 1}
        #Sets the joints to the specified velocitites
        cur_vel = limb.joint_velocities()
        limb.set_joint_torques(vel_0)
        print(cur_vel)
        limb.move_to_joint_positions(angles)
        time.sleep(1)
        limb.move_to_neutral()
        

    def understand(self):
        """
        Runs the left wave and right wave on two seperate threads
        """        
        threadLock = threading.Lock()
        threads = []

        # Create new threads
        thread1 = myThread(1, "leftwave", 1)
        thread2 = myThread(2, "rightwave", 2)

        # Start new Threads
        
        thread2.start()
        thread1.start()
        
        # Add threads to thread list
        threads.append(thread1)
        threads.append(thread2)

        # Wait for all threads to complete
        for t in threads:
            t.join()
        print "Exiting Main Thread"
        print("understand.")


    def dab(self):
        """
        Runs the left dab right dab and head dab on seperate threads
        """    
        threadLock = threading.Lock()
        threads = []

        # Create new threads
        thread1 = myThread(1, "leftdab", 1)
        thread2 = myThread(2, "rightdab", 2)
        thread3 = myThread(3, "headdab", 3)
        time.sleep(2.5)
        # Start new Threads
        thread1.start()
        thread2.start()
        thread3.start()

        # Add threads to thread list
        threads.append(thread1)
        threads.append(thread2)

        # Wait for all threads to complete
        for t in threads:
            t.join()
        print "Exiting Main Thread"
        print("understand.")
        

    def what(self):
        """
        Performs the what? gesture with a confused face
        """    
        threadLock = threading.Lock()
        threads = []

        # Create new threads
        thread1 = myThread(1, "leftwave", 1)
        thread2 = myThread(2, "rightwave", 2)
    

        # Start new Threads
        thread1.start()
        thread2.start()

        # Add threads to thread list
        threads.append(thread1)
        threads.append(thread2)

        # Wait for all threads to complete
        for t in threads:
            t.join()
        print "Exiting Main Thread"
        print("what.")

class myThread (threading.Thread):
    """
    This class has been used to setup gestures that need parellel execition such as 
    DAB and Understand Why?? (confused) gesture
    """
    # Intialsing the joints with the appropriate publish rate and control parameters
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
      self._pub_rate = rospy.Publisher('robot/joint_state_publish_rate',
                                         UInt16, queue_size=10)
      self._left_arm = baxter_interface.limb.Limb("left")
      self._right_arm = baxter_interface.limb.Limb("right")
      self._left_joint_names = self._left_arm.joint_names()
      self._right_joint_names = self._right_arm.joint_names()
      # control parameters
      self._rate = 500.0  # Hz
      print("Getting robot state... ")
      self._rs = baxter_interface.RobotEnable(CHECK_VERSION)
      self._init_state = self._rs.state().enabled
      print("Enabling robot... ")
      self._rs.enable()

        # set joint state publishing to 500Hz
        #self._pub_rate.publish(self._rate)
   def run(self):
    """
    The following functons have been made available so that
    the gestures can run in parellel
    For instance: To perform a dab the leftdab and rightdab are run at the same time
    """
      global threadLock
      print "Starting " + self.name
      if self.name =='leftwave':
            baxter_wave = Gesture()
            baxter_wave.leftarmwhy()

      if self.name =='rightwave':
            print "rightwave"
            baxter_wave = Gesture()
            baxter_wave.rightarmwhy()

      if self.name =='rightdab':
            print "rightdab"
            baxter_wave = Gesture()
            baxter_wave.rightdab()
      if self.name =='leftdab':
            print "leftdab"
            baxter_wave = Gesture()
            baxter_wave.leftdab()
      if self.name =='headdab':
            time.sleep(1.5)
            baxter_interface.Head().set_pan(-1.2)
            



   def set_neutral(self):
        """
        Sets both arms back into a neutral pose.
        """
        print("Moving to neutral pose...")
        self._left_arm.move_to_neutral()
        self._right_arm.move_to_neutral()    

def print_time(threadName, delay, counter):
   while counter:
      time.sleep(delay)
      print "%s: %s" % (threadName, time.ctime(time.time()))
      counter -= 1

threadLock =0;

def debug(str):
    print (str)

def speech_callback(data):
    """This callback function allows us to decide
    which gesture to perform based on speech to text data """
    print data.data
    if data.data=="wave":
         baxter_wave = Gesture()
         baxter_wave.wave()
    if data.data=="big wave":
         baxter_wave = Gesture()
         baxter_wave.bigwave()
    if data.data=="waiting":
         baxter_wave = Gesture()
         baxter_wave.sayno()
    if data.data=="hand shake":
         baxter_wave = Gesture()
         baxter_wave.shakehands()
    if data.data=="come here":
         baxter_wave = Gesture()
         baxter_wave.comehere()
    if data.data=="fist bump":
         baxter_wave = Gesture()
         baxter_wave.firstbump()
    if data.data=="high five":
         baxter_wave = Gesture()
         baxter_wave.highfive()
    if data.data=="dab":
         baxter_wave = Gesture()
         baxter_wave.dab()
         baxter_wave.set_neutral()
    if data.data=="what":
         baxter_wave = Gesture()
         baxter_wave.what()
         baxter_wave.set_neutral()
    if data.data=="come here":
        baxter_wave = Gesture()
        baxter_wave.comehere()
    if data.data=="understand":
        baxter_wave = Gesture()
        baxter_wave.nod()
    if data.data=="neutral":
        baxter_wave = Gesture()
        baxter_wave.set_neutral()


def main():
    """ Programming Project 1, A Gesture agent for Mobile Robot, 2018
        Author : Abhijeet S Nair (S3400586)
        Description : This script allows Baxter robot present in the VX LAB
        to perfrom gesture either based on audio input or perfom standalone gestures
        without any input simply by uncommtening the lines below.
    """
    global threadLock
    arg_fmt = argparse.RawDescriptionHelpFormatter  
    parser = argparse.ArgumentParser(formatter_class=arg_fmt,
                                     description=main.__doc__)
    parser.parse_args(rospy.myargv()[1:])
    
    # Currently uses the text to speech service
    print("Initializing node... ")
    rospy.init_node("guesta_gesture")
    baxter_wave = Gesture() #Gesture class contains all the gestures
    rospy.on_shutdown(baxter_wave.clean_shutdown)
    rospy.Subscriber("/input/speech/commands",String,speech_callback)
    rospy.spin()

    """
    All the gestures developed throughout the semester
    uncomment the code and run individual gestures for testing purposes
    """
    # baxter_wave.wave()
    # baxter_wave.bigwave()
    # baxter_wave.shakehands()
    # baxter_wave.firstbump()    
    # baxter_wave.highfive()    
    # baxter_wave.comehere()
    # baxter_wave.dab()
    # baxter_wave.nod()
    # baxter_wave.sayno()
    # baxter_wave.set_neutral()
    # baxter_wave.what()
    # baxter_wave.set_neutral()


if __name__ == '__main__':
    main()
