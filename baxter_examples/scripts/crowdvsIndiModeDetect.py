#!/usr/bin/env python

# RMIT UNIVERSITY, PROGRAMMING PROJECT-1, CAPSOTONE PROJECT
# PROJECT TITLE : A GESTURE AGENT FOR MOBILE ROBOT
# AUTHOR: ABHIJEET NAIR (S3400586)
# SUPERVISOR(S): DR. IAN PEAK (VX LAB, RMIT UNIVERSITY)
#               A/PROF. JAMES HARLAND (RMIT UNIVERSITY)
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted for educational use and study purposes only


import argparse
import random
import math
import time
import rospy

import baxter_interface

from baxter_interface import CHECK_VERSION
from std_msgs.msg import Float32
from std_msgs.msg import Float32MultiArray

globvar = 0
timer =0
start_time = time.time()

# Allows baxter to face individuals and objects around it 
class DetectIndividual(object):

    def __init__(self):
        """
        'Wobbles' the head
        """
        self._done = False
        self._head = baxter_interface.Head()

        # verify robot is enabled
        print("Getting robot state... ")
        self._rs = baxter_interface.RobotEnable(CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()
        print("Running. Ctrl-c to quit")

    def clean_shutdown(self):
        """
        Exits example cleanly by moving head to neutral position and
        maintaining start state
        """
        print("\nExiting example...")
        if self._done:
            self.set_neutral()
        if not self._init_state and self._rs.state().enabled:
            print("Disabling robot...")
            self._rs.disable()

    def set_neutral(self):
        """
        Sets the head back into a neutral pose
        """
        self._head.set_pan(0.0)
		
    def set_head(self,data):
        """
        Sets the head to angle passed by the callback
        """
        self._head.set_pan(data, speed=0.3, timeout=3)
   

def callback(data):
    """
    Primitive Function which worked only for individual mode
    This function has been replaced by angle_crowd_callback
    """
    radian = math.radians(data.data);
    radianFormatted = float("{0:.3f}".format(radian))
    wobbler = DetectIndividual()
    wobbler.set_head(radianFormatted-3.14)
    print radian-3.14


def angle_crowd_callback(data):
    """
    Callback for the data passed by the lidar and does two main things:
    1. Every 2.5 seconds detects new objects around the robot and faces it
    3. Otherwise looks at the average of all the angles given by the lidar (looks somewhere between/middle of the objects)
    """
    global start_time
    global timer
    avgAnglesEdited = data.data
    #print str(["{0:3.1f}".format(item) for item in avgAnglesEdited]).replace("'", "")
    wobbler = DetectIndividual()
    elapsed_time = time.time() - start_time
    print elapsed_time
    timer +=1
    #Timer loop that gets trigged every 2.5 seconds and make the robot look in the middle
    if (timer%4==0):
        #Go through all the elements in the avgAnglesEdited array that contain angle values and compute average
        for item in avgAnglesEdited:
            radian = math.radians(item);
            radianFormatted = float("{0:.3f}".format(radian))#format the data before setting the head position
            wobbler.set_head(radianFormatted-3.14) #set the head to the specified pos
            print timer
            #time.sleep(2)
    else:
        # Otherwise simply look at the new object given by the lidar
        global globvar  
        globvar=0
        counter=0
        for item in avgAnglesEdited:
            globvar= globvar+item
            counter=counter+1
        if (counter>0):
            average =globvar/counter;
            radian = math.radians(average);
            radianFormatted = float("{0:.3f}".format(radian))#format the data before setting the head position
            wobbler.set_head(radianFormatted-3.14) #set the head to the specified pos
            print average
            print radianFormatted
            print radianFormatted-3.14
            print counter



def main():
    """ Crowd mode vs Individual Mode Detection using LIDAR

    This script using LIDAR to distinguish between crowd and a single user
    and adjusts the robots head movements to face the individuals around it
    """
    arg_fmt = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=arg_fmt,
									 description=main.__doc__)
    parser.parse_args(rospy.myargv()[1:])
	

    # Intitize the node and register it with ROS Master
    print("Initializing node... ")
    rospy.init_node("guesta_crowd&Mastermode_detection", anonymous=True)
    # Subscribes to the data sent by LIDAR
    rospy.Subscriber("/input/lidar/angles",Float32MultiArray,angle_crowd_callback)
    time.sleep(1)
    rospy.spin() # keep checking for new data
	 


if __name__ == '__main__':
    main()
