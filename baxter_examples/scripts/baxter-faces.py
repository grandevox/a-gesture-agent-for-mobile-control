#!/usr/bin/env python

# RMIT UNIVERSITY, PROGRAMMING PROJECT-1, CAPSOTONE PROJECT
# PROJECT TITLE : A GESTURE AGENT FOR MOBILE ROBOT
# AUTHOR: ABHIJEET NAIR (S3400586)
# SUPERVISOR(S): DR. IAN PEAK (VX LAB, RMIT UNIVERSITY)
#               A/PROF. JAMES HARLAND (RMIT UNIVERSITY)
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted for educational use and study purposes only
import rospy
from baxter_interface import Limb
import baxter_interface
import time
import cv2
import cv_bridge
import rospkg
from sensor_msgs.msg import Image
import random
import rospy
from std_msgs.msg import String

#Publishes debug commands to pubdeug node
pubdebug = rospy.Publisher('/helper/speech_debug', String, queue_size=1)
#Publishes the images to xdisplay (baxter LCD display)
pub = rospy.Publisher('/robot/xdisplay', Image,latch=True, queue_size=10)

def callback(data):
	global msg
	global msg_closed
	if not data == msg_closed:
		msg = data

def debug(str):
    print(str)
    pubdebug.publish(str)

def faces(data):
    """
    Baxter has the capability to change faces based on the input
    given by the text to speech topic
    """
    debug("Speaking: "+str(data.data))
    text = data.data
    
    #If the command has the text big and wave in it then smile
    if "big" in text and "wave" in text:
        debug("Face: Hello")
        pub.publish(msg_happy)
    
    #If the command has hello in it then be surprised
    elif "hello" in text:
        debug("Face: Hello")
        pub.publish(msg_surprised)
        data=""
        text=""
        
    #If the command has hand shake in it then smile
    elif "hand" in text and "shake" in text:
        debug("Face: Hand Shake")
        pub.publish(msg_happy)
    
    #If the command has wave in it then be happy
    elif "wave" in text:
        debug("Face: Hi")
        pub.publish(msg_happy)

    #If the command has dab in it then smile
    elif "dab" in text:
        debug("Face: Dab")
        pub.publish(msg_happy)
        #pub.publish(msg_happy)

    #If the command has fist bump in it then smile
    elif "fist" in text and "bump" in text:
        debug("Face: fist Bump")
        pub.publish(msg_happy)

    #If the command has understand in it then pose confused
    elif "understand" in text:
        debug("Face: Understand")
        pub.publish(msg_confused)
        

    elif "come" in text or "here" in text:
        debug("Face: Challenged Accpeted")
        
    #If the command has high five in it smile
    elif "high" in text and "five" in text:
        debug("Face: High Five")
        pub.publish(msg_happy)

    elif "stop" in text:
        debug("Face: See_you_next_time")
       
    else: 
        debug("Face: Dont understand")
        pub.publish(msg_confused)

        
        
try:
    """
    Intilases the variables with the images found from baxter-faces-library at 
    https://github.com/nfitter/BaxterFaces. The expressions used from the library were
    sad, happy, surprised , confused, neutral
    """
    rospy.init_node('guesta_faceExpressions')
    _images = '/home/guesta/demo_ws/src/baxter_examples/share/baxter-eyes-master'

    img = cv2.imread(_images + '/straight.jpg')
    sub = rospy.Subscriber('/robot/xdisplay', Image, callback)

    img_closed = cv2.imread(_images + '/closed.jpg')
    msg_closed = cv_bridge.CvBridge().cv2_to_imgmsg(img_closed)
    msg = cv_bridge.CvBridge().cv2_to_imgmsg(img)

    img_happy = cv2.imread(_images + '/happy.jpg')
    msg_happy =cv_bridge.CvBridge().cv2_to_imgmsg(img_happy);

    img_sad = cv2.imread(_images + '/sad.jpg')
    msg_sad =cv_bridge.CvBridge().cv2_to_imgmsg(img_sad);

    img_confused = cv2.imread(_images + '/confused.jpg')
    msg_confused =cv_bridge.CvBridge().cv2_to_imgmsg(img_confused);

    img_surprised = cv2.imread(_images + '/surprised.jpg')
    msg_surprised =cv_bridge.CvBridge().cv2_to_imgmsg(img_surprised);
    
    img_neutral = cv2.imread(_images + '/RedNeutralBaxterFace.jpg')
    msg_neutral =cv_bridge.CvBridge().cv2_to_imgmsg(img_neutral);
   

    while not rospy.is_shutdown():
        #Changes face continously from being happy to neutral 
        pub.publish(msg_neutral)
        timer = random.randint(2,4)	
        #Faces becomes happy
        time.sleep(timer)
        print "happy"
        pub.publish(msg_happy)
        #Faces becomes neutral
        timer = random.uniform(0.2,0.5)	
        time.sleep(timer)
        pub.publish(msg_neutral)
        #rospy.init_node('guesta_speech', anonymous=False)
        rospy.Subscriber("/input/speech/commands", String, faces) #Changes face based on the speeech commands
        debug("starting")
    #rospy.spin()
    
except KeyboardInterrupt:
    pass
    

