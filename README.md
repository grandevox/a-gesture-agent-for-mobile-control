# **A Gesture Agent for Mobile Control**

In this project we developed a gesture library for RED Robot present in the VX LAB at RMIT UNIVERSITY, MELBOURNE, AUSTRALIA.

The library currently has the following gestures :

- **WAVE:**
The robot lifts its right arm and moves its wrist pitch joint (W1) side by side to say "Hello". The robot moves its wrist pitch (W1) side by side three times consecutively.

- **BIG WAVE:**
The big wave gesture is an exaggerated wave which involves the left arm to completely rise to the top and move the wrist pitch(W1) from left to right 3 times consecutively.

- **SHAKE HANDS:**
This is similar to a normal handshake where the robot gives its wrist-pitch joint to the human for a brief up and down. After a short span of two seconds, the robot retrieves its arm and brings it back to the neutral position.

- **FIST BUMP:**
Similar to a handshake the robot moves its fist away (wrist pitch) from its body and waits for a couple of seconds before a human can bump its fist and then retrieves its arm.

- **HIGH FIVE:**
The robot raises its arm to about the height of its head and uses its wrist joint as a palm surface where humans can hit their palm to perform a high-five.

- **COME HERE:**
The come here gesture is when the robot aligns its arm parallel to the ground surface (floor) and moves its wrist joint (wrist pitch) back and forth (from 0 deg (parallel to the floor) to 110 deg (perpendicular to the floor)). This gesture is aimed at calling the user towards the robot.

- **DAB:**
The robot performs this simple dance move where it moves the head close to the bent crook of a slanted arm and raises the opposite arm in the parallel direction, both arms are pointed to the side and at an upward angle, in a way that both arms are almost parallel to each other.

- **NOD:**
In this mode, the robot uses the head to confirm to the user that a specific command has been received and the robot understands this command. The robot nods its head to say moves it up and down) so that its clear to the user that the command has been interpreted correctly.

- **SAY NO:**
The robot shakes its head from -85 degrees (extreme left) to +85 deg (extreme right) to say no or refuse. It does this twice consecutively.

- **"CONFUSED" OR "WHAT?" GESTURE:**
In this gesture, the robot raises both its arm with its wrist joint completely flat to the ground surface and elbow at an angle looking straight at the user and posing confused.

---

## NOTE:
All of the above mentioned physical movements (gestures) are supplemented with an audio output and facial expressions (which are specific to each gesture) and get played concurrently when performing the gesture. In order to view these gestures please locate the **gesture videos** folder in the git repository.
